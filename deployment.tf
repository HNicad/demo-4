resource "kubernetes_deployment" "petclinic-db" {
    
    metadata {
      name = var.db_instance_name
    }

    spec {
        replicas = var.db_replica_count

        selector {
            match_labels = {
                app = var.db_instance_name
            }
        }
        
        template {
            metadata {
                labels = {
                    app = var.db_instance_name
                 }
            }
            spec {
                container{
                    image = var.db_image
                    name = var.db_instance_name
                    port {
                        container_port = var.db_container_port
                    }
                    env {
                        name = "MYSQL_ROOT_PASSWORD"
                        value = var.mysql_root_password
                    }
                    env {
                        name = "MYSQL_USER"
                        value = var.mysql_user
                    }
                    env {
                        name = "MYSQL_PASSWORD"
                        value = var.mysql_password
                    }
                    env {
                        name = "MYSQL_DATABASE"
                        value = var.mysql_db_name
                    }
                    
                }

            }
        }

    }
}


resource "kubernetes_deployment" "petclinic-app" {
    metadata {
      name = var.app_instance_name
    }

    spec {
        replicas = var.app_replica_count

        selector {
            match_labels = {
                app = var.app_instance_name
            }
        }
        
        template {
            metadata {
                labels = {
                    app = var.app_instance_name
                 }
            }
            spec {
                container{
                    image = var.app_image
                    name = var.app_instance_name
                    port {
                        container_port = var.app_container_port
                    }
                    env {
                        name = "MYSQL_USER"
                        value = var.mysql_user
                    }
                    env {
                        name = "MYSQL_PASSWORD"
                        value = var.mysql_password
                    }
                    env {
                        name = "MYSQL_URL"
                        value = "jdbc:mysql://${kubernetes_service.petclinic_db.metadata.0.name}/${var.mysql_db_name}"
                    }
                }

            }
        }
    }
    depends_on = [
      kubernetes_deployment.petclinic-db,
      kubernetes_service.petclinic_db
    ]
}