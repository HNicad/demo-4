resource "kubernetes_service" "petclinic_db" {
    metadata {
        name = var.db_instance_name
    }
    spec {
        selector = {
            app = var.db_instance_name
        }
        port {
            protocol = "TCP"
            port = var.k8s_db_service_port
            target_port = var.k8s_db_service_target_port
        }
    }
}



resource "kubernetes_service" "petclinic_app" {
    metadata {
        name = var.app_instance_name
    }

    spec {
        selector = {
            app = var.app_instance_name
        }
        port {
            protocol = "TCP"
            port = var.k8s_app_service_port
            target_port = var.k8s_app_service_target_port
        }
        type = var.app_service_type
    } 
}