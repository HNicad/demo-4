variable "cluster_name" {
    default = "cluster-1"
    description = "Name of the cluster"  
}
variable "cluster_version" {
    default = "1.20"
    description = "Version of k8s in cluster"
}

variable "vpc_name" {
    default = "k8s-vpc"
}

variable "vpc_cidr" {
    default = "172.16.0.0/16"
}

variable "vpc_private_subnets" {
    type = list
    default = ["172.16.1.0/24", "172.16.2.0/24", "172.16.3.0/24"]
}

variable "vpc_public_subnets" {
    type = list
    default = ["172.16.4.0/24", "172.16.5.0/24", "172.16.6.0/24"]
}

variable "eks_node_desired_capacity" {
    default = 1
}

variable "eks_node_max_capacity" {
    default = 10
}

variable "eks_node_min_capacity" {
    default = 1
}

variable "eks_node_instance_type" {
    default = "t3.large"
}

variable "app_instance_name" {
    default = "petclinic-app"
}

variable "db_instance_name" {
    default = "petclinic-db"
}

variable "app_image" {
    default = "hnijad/app:latest"
}

variable "db_image" {
    default = "hnijad/db:latest"
}

variable "mysql_user" {
    default = "petclinic"
}

variable "mysql_password" {
    default = "petclinic"
    sensitive = true
}
variable "mysql_db_name" {
    default = "petclinic"
}

variable "mysql_root_password" {
    default = "root"
    sensitive = true
}

variable "app_replica_count" {
    default = 1
}

variable "db_replica_count" {
    default = 1
}

variable "app_container_port" {
    default = 8080
}

variable "db_container_port" {
    default = 81
}

variable "k8s_db_service_port" {
    default = 3306
}

variable "k8s_db_service_target_port" {
    default = 3306
}
variable "k8s_app_service_port" {
    default = 80
}

variable "k8s_app_service_target_port" {
    default = 8080
}

variable "app_service_type" {
    default = "LoadBalancer"
}